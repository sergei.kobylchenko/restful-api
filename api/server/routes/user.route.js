const { Router } = require('express');
const UserController = require('../controllers/user.controller');

const route = Router();

route.get('/:id', UserController.getUser);
route.post('/users', UserController.addUser)

module.exports = route;