const { User, Profile, Invites, Rooms } = require('../src/models')

class UserService {
  static async addUser(newUser) {
    try {
      return await User.create(newUser)
    } catch (error) {
      throw error
    }
  }

  static async removeUser() {
    try {
      
    } catch (error) {
      throw error
    }
  }

  static async updateUser() {
    try {
      
    } catch (error) {
      throw error
    }
  }

  static async getAllUsers() {
    try {
      
    } catch (error) {
      throw error
    }
  }

  static async getOneUser(key, value) {
    try {
      return await User.findOne({ 
        where: { [key]: Number(value) },
        // include: []
        // attributes: [ 'id', 'email', 'hashedPass', 'inviteId' ]
      })
    } catch (error) {
      throw error
    }
  }
}

module.exports = UserService;
