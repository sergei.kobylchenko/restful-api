const UserService = require('../services/user.service')

class UserController {
  static async getUser(req, res) {
    const { id } = req.params;
    try {
      const user = await UserService.getOneUser('id', id)

      res.json(user)
    } catch (error) {
      //send error
    }
  }

  static async addUser(req, res) {
    const user = req.body

    try {
      const newUser = await UserService.addUser(user)
      return res.status(201).json(newUser)
    } catch (error) {
      
    }
  }
}

module.exports = UserController;