'use strict';
module.exports = (sequelize, DataTypes) => {
  const Profile = sequelize.define('Profile', {
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    address: DataTypes.STRING,
    birthday: DataTypes.STRING,
    linkedInURL: DataTypes.STRING
  }, {});
  Profile.associate = function(models) {
    Profile.belongsTo(models.User)
  };
  return Profile;
};