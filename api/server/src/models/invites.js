'use strict';
module.exports = (sequelize, DataTypes) => {
  const Invites = sequelize.define('Invites', {
    company: DataTypes.STRING,
    code: DataTypes.NUMBER
  }, {});
  Invites.associate = function(models) {
    Invites.hasMany(models.User, {
      foreignKey: 'inviteId',
    })
  };
  return Invites;
};