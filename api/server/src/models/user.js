'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: DataTypes.STRING,
    hashedPass: DataTypes.STRING
  }, {});
  User.associate = function(models) {
    User.hasOne(models.Profile),
    User.belongsTo(models.Invites, {
      foreignKey: 'inviteId',
    });
    User.belongsToMany(models.Rooms, { 
      through: models.Members,
    })
  };
  return User;
};