'use strict';
module.exports = (sequelize, DataTypes) => {
  const Rooms = sequelize.define('Rooms', {
    name: DataTypes.STRING
  }, {});
  Rooms.associate = function(models) {
    Rooms.belongsToMany(models.User, { through: models.Members})
  };
  return Rooms;
};