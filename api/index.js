const express = require('express');
const userRouter = require('./server/routes/user.route')
const app = express();

app.use(express.json({ extended: true }))

app.use('/api', userRouter)

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
  console.log(`Server is running on PORT ${PORT}`);
});